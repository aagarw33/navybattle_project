package com.ttheissi.navy_battle;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.ttheissi.navy_battle.server.ServerLayer;

import java.util.List;

public class ChooseOpponent extends Activity implements View.OnClickListener {

    //Background Music Variables//
    private boolean mIsBound = false;
    public Music mServ;
    Animation right,left;
  //  ImageButton buttonMute;

    /** BUTTONS **/
        Button buttonRandomOpponent;
        Button buttonPlayWithAFriend;

    /** BUTTONS **/
    /** FRAGMENT **/
        Fragment listPreviousOpponents;
    /** FRAGMENT **/

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_opponent);

        /** BUTTONS **/
            buttonRandomOpponent = (Button) findViewById(R.id.buttonRandomOpponent);
            buttonRandomOpponent.setOnClickListener(this);
            buttonPlayWithAFriend = (Button) findViewById(R.id.buttonPlayWithAFriend);
            buttonPlayWithAFriend.setOnClickListener(this);
        /** BUTTONS **/
        /** FRAGMENT **/
            Fragment fragment = (Fragment) getFragmentManager().findFragmentById(R.id.listFragmentPreviousOpponents);
        /** FRAGMENT **/

        /**Animation**/
            right = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_random);
            left = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_radom_left);
    }

    //Background Music Life Cycle

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startService(new Intent(this, Music.class));

    }

    @Override
    protected void onStop() {
        super.onStop();
       // if(mServ!=null){
            doUnbindService();
          //  mServ = null;
          //  if(isFinishing()){
                stopService(new Intent(this,Music.class));
          //  }
       // }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this,Music.class));
        doBindService();
    }


    @Override
    protected void onPause() {
        super.onPause();
      /*  if(this.isFinishing())
        {
            stopService(new Intent(this,Music.class));
            doUnbindService();
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.choose_opponent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) { //DO NOT FORGET TO BREAK AT THE END OF EACH STATEMENT
        switch(view.getId()) {
            case R.id.buttonRandomOpponent:
                ServerLayer serverLayer = ServerLayer.getInstance();
                serverLayer.requestRandomOpponent();
                Toast.makeText(this,"Requesting Random Opponent",Toast.LENGTH_LONG).show();
                break;

            case R.id.buttonPlayWithAFriend:
                Intent searchFriend = new Intent(this, FindFriend.class);
                buttonPlayWithAFriend.startAnimation(left);
                startActivity(searchFriend);
                break;
        }
    }

//Binding the Background Music Service to the Activity
    public ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };


    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
    }
}
