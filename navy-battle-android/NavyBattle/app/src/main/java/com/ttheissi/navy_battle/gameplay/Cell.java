package com.ttheissi.navy_battle.gameplay;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

import com.ttheissi.navy_battle.R;
import com.ttheissi.navy_battle.gameplay.ships.Battleship;
import com.ttheissi.navy_battle.gameplay.ships.Carrier;
import com.ttheissi.navy_battle.gameplay.ships.Cruiser;
import com.ttheissi.navy_battle.gameplay.ships.None;
import com.ttheissi.navy_battle.gameplay.ships.Placeable;
import com.ttheissi.navy_battle.gameplay.ships.Scout;
import com.ttheissi.navy_battle.gameplay.ships.Submarine;

/**
 * Created by Aaron on 4/17/2014.
 */
public class Cell {
    CellState state;
    Placeable ship;
    float leftBorder,rightBorder,topBorder,bottomBorder;
    boolean hasShip;
    boolean goingToBePlaced;

    public Cell() {
        state = CellState.Water;
        leftBorder=0;
        rightBorder=0;
        topBorder=0;
        bottomBorder=0;
        ship = new None();
        hasShip = false;
        goingToBePlaced = false;
    }

    /** Instantiate based on toString function */
    public Cell(String fromString) {
        String[] values = fromString.split("__");
        state = CellState.valueOf(values[0]);
        // Create ship
        switch (Integer.valueOf(values[1])) {
            case R.drawable.battleship:
            case R.drawable.battleshiprot:
                ship = new Battleship();
                break;
            case R.drawable.carrier:
            case R.drawable.carrierrot:
                ship = new Carrier();
                break;
            case R.drawable.scout:
            case R.drawable.scoutrot:
                ship = new Scout();
                break;
            case R.drawable.none:
                ship = new None();
                break;
            case R.drawable.sub:
            case R.drawable.subrot:
                ship = new Submarine();
                break;
            case R.drawable.cruiser:
            case R.drawable.cruiserrot:
                ship = new Cruiser();
                break;
            default:
                ship = new None();
                throw new RuntimeException("Bad ship ID!");
        }
        hasShip = Boolean.valueOf(values[2]);
        leftBorder = Float.valueOf(values[3]);
        rightBorder = Float.valueOf(values[4]);
        topBorder = Float.valueOf(values[5]);
        bottomBorder = Float.valueOf(values[6]);
        goingToBePlaced = Boolean.valueOf(values[7]);
    }

    public float getLeftBorder() {
        return leftBorder;
    }
    public float getRightBorder() {
        return rightBorder;
    }
    public float getTopBorder() {
        return topBorder;
    }
    public float getBottomBorder() {
        return bottomBorder;
    }
    public CellState getCellState(){
        return state;
    }
    public Placeable getShip(){
        return ship;
    }
    public boolean getGoingToBePlaced() { return goingToBePlaced;}
    public boolean getHasShip(){return hasShip;}
    public void setCellState(CellState nState){
        state = nState;
    }
    public void setShip(Placeable ship){
        this.ship = ship;
    }
    public void setHasShip(boolean hasShip){
        this.hasShip = hasShip;
    }
    public void setGoingToBePlaced(boolean aBool) { goingToBePlaced = aBool; }
    public void setBorders(float top, float bottom, float left, float right){
        topBorder = top;
        bottomBorder = bottom;
        leftBorder = left;
        rightBorder = right;
    }

    public String toString() {
        return state.toString() + "__" + ship.getDrawId() + "__" + hasShip + "__" + leftBorder + "__" +
                rightBorder + "__" + topBorder + "__" + bottomBorder + "__" + goingToBePlaced;
    }

    public boolean contains(Point coord) {
        //if(ships){
        if (leftBorder <= coord.x && coord.x <= rightBorder) {
            if (topBorder <= coord.y && coord.y <= bottomBorder) {
                if (hasShip) {
                    state = CellState.Hit;
                } else {
                    state = CellState.Miss;
                }
                return true;
            }
        }
        return false;
    }
       // }


        /*else{
            if(leftBorder<=coord.x && coord.x<=rightBorder){
                if (topBorder<=coord.y && coord.y<=bottomBorder){
                    hasShip = true;
                    return true;
                }
            }
        }
        return false;
    }*/

    public void onDraw(Canvas canvas, Paint paint){
        canvas.drawRect(leftBorder,topBorder,rightBorder,bottomBorder,paint);
    }
}
