package com.ttheissi.navy_battle.gameplay.ships;

import com.ttheissi.navy_battle.Game;
import com.ttheissi.navy_battle.R;

/**
 * Created by aaron on 5/9/14.
 */
public class Carrier implements Placeable {

    private int rot = 0;

    public Carrier() {
        rot = Game.getRot();
    }

    @Override
    public int getSize() {
        return 4;
    }

    @Override
    public int getDrawId() {
        if (rot == -90) return R.drawable.carrierrot;
        else return R.drawable.carrier;
    }

    @Override
    public String getDisplayName() {
        return "carrier";
    }

    @Override
    public int getRot() {
        return rot;
    }

    @Override
    public void setRot(int rot) {
        this.rot = rot;
    }
}
