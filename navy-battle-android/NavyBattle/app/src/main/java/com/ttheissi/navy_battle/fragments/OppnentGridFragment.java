package com.ttheissi.navy_battle.fragments;



import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ttheissi.navy_battle.GridView;
import com.ttheissi.navy_battle.R;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 *
 */
public class OppnentGridFragment extends Fragment {

    GridView grid;
    Point click;

    public OppnentGridFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_grid,container,false);

        return v;
    }
}
