package com.ttheissi.navy_battle;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.ttheissi.navy_battle.gameplay.GameInstance;

public class LaunchGame extends Activity implements View.OnClickListener{

    /** BUTTONS **/
    Button buttonPlay;
    Button buttonOptions;
    Button buttonQuit;
    Animation zoom;
    /** BUTTONS **/

    //Background Music Variables//
    private boolean mIsBound = false;
    public Music mServ;

    GameInstance gameInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_game);

        /** BUTTONS **/
        buttonPlay = (Button) findViewById(R.id.buttonStartGame);
        buttonPlay.setOnClickListener(this);
        buttonOptions = (Button) findViewById(R.id.buttonInfo);
        buttonOptions.setOnClickListener(this);
        buttonQuit = (Button) findViewById(R.id.buttonQuit);
        buttonQuit.setOnClickListener(this);
        /** BUTTONS **/
        if( getIntent().getExtras() != null) {
            Intent intent = getIntent();
            if (!intent.getExtras().containsKey("GameInstance"))
                throw new RuntimeException("No Game Chosen!");

            gameInstance = GameInstance.fromString(intent.getExtras().getString("GameInstance"));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.launch_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //Background Music Life Cycle//
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startService(new Intent(this, Music.class));

    }
    @Override
    protected void onStop() {
        super.onStop();
        if(mServ!=null){
            doUnbindService();
            mServ = null;
            if(isFinishing()){
                stopService(new Intent(this,Music.class));
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this,Music.class));
        doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    public void onClick(View view) { //DO NOT FORGET TO BREAK AT THE END OF EACH STATEMENT
        switch(view.getId()) {
            case R.id.buttonStartGame:
                Intent GameView = new Intent(this, Game.class);
                GameView.putExtra("GameInstance", gameInstance.toString());
                startActivity(GameView);
                break;

            case R.id.buttonInfo:
                Intent result = new Intent(this,OpponentInfo.class);
                result.putExtra("GameInstance", gameInstance.toString());
                startActivity(result);
                break;

            case R.id.buttonQuit:
               Intent in = new Intent(getApplicationContext(),MainActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.putExtra("EXIT",true);
                startActivity(in);
                break;
        }
    }

    //Binding the Background Music Service to the Activity
    public ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
    }
}
