package com.ttheissi.navy_battle.gameplay;

/**
 * Created by Aaron on 3/24/2014.
 */
public enum GameStatus {
    InProgress,
    Completed,
    Pending;
}