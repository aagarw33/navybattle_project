package com.ttheissi.navy_battle.gameplay;

/**
 * Created by Aaron on 3/24/2014.
 *
 * Defines the current state of a game
 */

public class GameInstance {
    private Player enemy;
    private GameStatus gameStatus;
    private GameState gameState;

    public GameInstance(Player anEnemy, GameStatus aGameStatus, GameState aGameState) {
        enemy = anEnemy;
        gameStatus = aGameStatus;
        gameState = aGameState;
    }

    public GameInstance() {

    }

    /**
     * ***** GETTERS/SETTERS ****************
     */
    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public Player getEnemy() {
        return enemy;
    }

    public void setEnemy(Player enemy) {
        this.enemy = enemy;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public static GameInstance fromString(String gameInstanceString) {
        String[] split = gameInstanceString.split("::::");
        Player enemy = Player.fromString(split[0]);
        GameStatus status = GameStatus.valueOf(split[1]);
        GameState state = GameState.fromString(split[2]);
        return new GameInstance(enemy, status, state);
    }

    @Override
    public String toString() {
        return enemy.toString() + "::::" + gameStatus.toString() + "::::" + gameState.toString();
    }
}
