package com.ttheissi.navy_battle;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ttheissi.navy_battle.fragments.ControlsFragment;
import com.ttheissi.navy_battle.gameplay.Cell;
import com.ttheissi.navy_battle.gameplay.CellState;
import com.ttheissi.navy_battle.gameplay.GameState;
import com.ttheissi.navy_battle.gameplay.ships.Battleship;
import com.ttheissi.navy_battle.gameplay.ships.Carrier;
import com.ttheissi.navy_battle.gameplay.ships.Cruiser;
import com.ttheissi.navy_battle.gameplay.ships.Placeable;
import com.ttheissi.navy_battle.gameplay.ships.Scout;
import com.ttheissi.navy_battle.gameplay.ships.Submarine;
import com.ttheissi.navy_battle.server.ServerLayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thomas on 3/27/14.
 */
public class GridView extends View {
    Point click;
    Point placeShip;
    static float degrees;
    DisplayMetrics metrics;
    float sWidth, sHeight;
    Paint outline = null;
    Paint hit = null;
    Paint miss = null;
    Cell[][] grid = new Cell[10][10];

    public GridView(Context context, AttributeSet attrs) {
        super(context,attrs);
        for(int i=0;i<10;i++){
            for(int j=0;j<10;j++){
                grid[i][j]= new Cell();
            }
        }
    }

    private void init(){
        outline = new Paint(0);
        outline.setStyle(Paint.Style.STROKE);
        hit = new Paint(0);
        miss = new Paint(0);
        hit.setStyle(Paint.Style.STROKE);
        miss.setStyle(Paint.Style.STROKE);
        outline.setStrokeWidth(3);
        hit.setStrokeWidth(3);
        miss.setStrokeWidth(3);
        outline.setColor(Color.BLACK);
        hit.setColor(Color.RED);
        miss.setColor(Color.BLUE);


        sHeight = getMeasuredHeight();
        sWidth = getMeasuredWidth();
        Resources resources = getContext().getResources();
        metrics = resources.getDisplayMetrics();

        float increment = .1f*sWidth;
        float up=0.0f, down=increment, left=0.0f, right=increment;

        for(int i=0; i<10;i++){
            for(int j=0;j<10;j++){
                grid[i][j].setBorders(up,down,left,right);
                left+=increment;
                right+=increment;
            }
            down+=increment;
            up+=increment;
            right = increment;
            left = 0;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // The compass is a circle that fills as much space as possible.
        // Set the measured dimensions by figuring out the shortest boundary,
        // height or width.
        int measuredWidth = measure(widthMeasureSpec);
        int measuredHeight = measure(heightMeasureSpec);
        int d = Math.min(measuredHeight, measuredWidth);

        setMeasuredDimension(d, d);
    }

    private int measure(int measureSpec) {
        int result;

        // Decode the measurement specifications.
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.UNSPECIFIED) {
            // Return a default size of 200 if no bounds are specified.
            result = 200;
        } else {
            // As you want to fill the available space
            // always return the full available bounds.
            result = specSize;
        }
        return result;
    }

    public void click(){
        for(int i=0; i<10;i++){
            for(int j=0;j<10;j++){
                if(grid[i][j].contains(click)){
                    ServerLayer serverLayer = ServerLayer.getInstance();
                    serverLayer.attackCell(null, i, j);
                    //Toast.makeText(getContext(), "coords " + i + " " + j, Toast.LENGTH_SHORT).show();
                    grid[i][j].setGoingToBePlaced(true);
                    switch (Game.getShipType()) {
                        case 0:
                            grid[i][j].setShip(new Carrier());
                            break;
                        case 1:
                            grid[i][j].setShip(new Battleship());
                            break;
                        case 2:
                            grid[i][j].setShip(new Cruiser());
                            break;
                        case 3:
                            grid[i][j].setShip(new Submarine());
                            break;
                        case 4:
                            grid[i][j].setShip(new Scout());
                            break;
                        default:
                            grid[i][j].setShip(new Battleship());
                    }
                    //grid[i][j].onDraw(canvas, waterPaint);
                    super.postInvalidate();
                    return;
                }
            }
        }

        Toast.makeText(getContext(), "out of bounds", Toast.LENGTH_SHORT).show();
    }

    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        init();
        Paint waterPaint = new Paint();
        waterPaint.setColor(Color.CYAN);

        waterPaint.setAlpha(40);
        for(int i=0; i<10;i++){
            for(int j=0;j<10;j++) {
                Placeable ship = grid[i][j].getShip();
                RectF draw = new RectF(grid[i][j].getLeftBorder(), grid[i][j].getTopBorder() + ((grid[i][j].getTopBorder() - grid[i][j].getBottomBorder()) * (ship.getSize() - 1)), grid[i][j].getRightBorder(), grid[i][j].getBottomBorder());
                Matrix drawMatrix = new Matrix();
                drawMatrix.reset();
                drawMatrix.postRotate(ship.getRot(), grid[i][j].getLeftBorder(), grid[i][j].getTopBorder());
                if (ship.getRot() != 0)
                    drawMatrix.postTranslate(0, -(grid[i][j].getTopBorder()-grid[i][j].getBottomBorder()));
                drawMatrix.mapRect(draw);

                if (grid[i][j].getGoingToBePlaced() == true) {

                    Paint choosen = new Paint();
                    choosen.setColor(Color.RED);
                    choosen.setStyle(Paint.Style.STROKE);
                    grid[i][j].onDraw(canvas, choosen);
                    canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), ship.getDrawId()), null, draw, null);
                    outline.setColor(Color.RED);
                    grid[i][j].onDraw(canvas, outline);
                    outline.setColor(Color.BLACK);
                }
                if((grid[i][j].getCellState() == CellState.Water) && (grid[i][j].getGoingToBePlaced() == false)){
                    grid[i][j].onDraw(canvas, waterPaint);
                    canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), ship.getDrawId()), null, draw, null);
                    grid[i][j].onDraw(canvas, outline);
                }
                else if((grid[i][j].getCellState() == CellState.Hit) && (grid[i][j].getHasShip()) && (grid[i][j].getGoingToBePlaced() == false)) {
                    grid[i][j].onDraw(canvas, waterPaint);
                    canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), ship.getDrawId()), null, draw, null);
                    grid[i][j].onDraw(canvas, hit);
                }
                else {
                    grid[i][j].onDraw(canvas, waterPaint);
                    canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), ship.getDrawId()), null, draw, null);
                    grid[i][j].onDraw(canvas, miss);
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event){
        super.onTouchEvent(event);

        if (event == null) return false;
        click = new Point();
        click.x = (int)event.getX();
        click.y = (int)event.getY();
        click();

        return true;
    }


    public void shipToCell(int numCell, float degrees) {
        Toast.makeText(getContext(), "Communication successful", Toast.LENGTH_SHORT).show();
    }

}