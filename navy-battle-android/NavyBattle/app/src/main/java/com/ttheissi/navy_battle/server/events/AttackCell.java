package com.ttheissi.navy_battle.server.events;

import android.os.AsyncTask;

import com.ttheissi.navy_battle.gameplay.GameInstance;
import com.ttheissi.navy_battle.gameplay.Player;
import com.ttheissi.navy_battle.server.Event;
import com.ttheissi.navy_battle.server.EventType;
import com.ttheissi.navy_battle.server.ServerLayer;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.Calendar;

/**
 * Created by aaron on 4/20/14.
 */
public class AttackCell extends AsyncTask<Object, Integer, Event> {

    @Override
    protected Event doInBackground(Object... params) {
        GameInstance gameInstance = (GameInstance) params[0];
        Integer posx = (Integer) params[1];
        Integer posy = (Integer) params[2];

        JSONObject jsonObject = new JSONObject();
        Event response = null;
        try {
            jsonObject.put("EventType", EventType.GameAction.toString());
            jsonObject.put("EventTime", Calendar.getInstance().getTimeInMillis());
            jsonObject.put("ActionType", "AttackCell");
            jsonObject.put("CellPosX", posx);
            jsonObject.put("CellPosY", posy);
            jsonObject.put("GameUID", gameInstance.getGameState().getGameUID());

            // try six times before giving up
            InputStream result = ServerLayer.request(jsonObject);
            for (int i = 0; i < 5; i++)
                if (result == null)
                    result = ServerLayer.request(jsonObject);
                else break;

            if (result != null)
                return ServerLayer.eventFromStream(ServerLayer.request(jsonObject));
            else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void onPostExecute(Event result) {
        if (result != null)
            ServerLayer.getInstance().returnFromAsync(result);
    }
}
