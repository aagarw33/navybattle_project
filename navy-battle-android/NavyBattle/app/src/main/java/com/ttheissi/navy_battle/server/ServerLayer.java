package com.ttheissi.navy_battle.server;

import android.widget.Toast;

import com.ttheissi.navy_battle.gameplay.GameInstance;
import com.ttheissi.navy_battle.gameplay.Player;
import com.ttheissi.navy_battle.server.events.AttackCell;
import com.ttheissi.navy_battle.server.events.OpponentRequest;
import com.ttheissi.navy_battle.server.events.RandomOpponentRequest;
import com.ttheissi.navy_battle.server.events.UpdateRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Aaron on 3/23/14.
 *
 * This class acts as a buffer between the game and the server code
 * Most methods will take time to return, so execute them in a separate thread
 */
public class ServerLayer {

    private static final String serverURL = "http://64.131.107.244";
    private static final String charset = "UTF-8";
    private Queue<Event> eventSpool = new ConcurrentLinkedQueue<Event>();
    private boolean hasEvents = false;
    private Player player = null;

    private static ServerLayer instance = null;

    /** Use ServerLayer.getInstance() */
    protected ServerLayer() {}

    /** Called once to give the server layer the player info */
    public void init(Player player) { this.player = player; }

    public static ServerLayer getInstance() {
        if (instance == null) instance = new ServerLayer();

        return instance;
    }

    /**
     * Gets the latest updates from the server for the given player. Should be user!
     *
     * @return Event[], an array of events since last recieved update
     */
    public Event[] getUpdates() {
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.execute(player);
        if (hasEvents) {
            List<Event> events = new ArrayList<Event>();
            do {
                events.add(eventSpool.poll());
            } while (eventSpool.peek() != null);
            return events.toArray(new Event[0]);
        } else return new Event[]{};
    }

    public Player getPlayer() { return player; }

    /** Places a given event into the event spool, sets hasEvents to true */
    public void returnFromAsync(Event event) {
        eventSpool.add(event);
        hasEvents = true;
    }

    /**
     * Attempts to get a random opponent from the server
     *
     * @return an Event containing the result of the operation
     */
    public void requestRandomOpponent() {
        RandomOpponentRequest request = new RandomOpponentRequest();
        request.execute(player);
    }

    /**
     * Requests a game with the specified opponent
     *
     * @param opponentUID
     */
    public void requestOpponent(String opponentUID) {
        OpponentRequest request = new OpponentRequest();
        request.execute(player.getUID().toString(), player.getName(), opponentUID);
    }

    /**
     * Attacks a cell
     */
    public void attackCell(GameInstance gameInstance, int x, int y) {
        AttackCell attack = new AttackCell();
        attack.execute(gameInstance, Integer.valueOf(x), Integer.valueOf(y));
    }

    /** Processes an input stream as an Event and returns it */
    public static Event eventFromStream(InputStream inputStream) {
        java.util.Scanner s = new java.util.Scanner(inputStream, charset).useDelimiter("\\A");
        String result =  s.hasNext() ? s.next() : "";
        Event event = new Event();

        try {
            JSONObject jsonObject = new JSONObject(new JSONTokener(result));
            if (!jsonObject.has("EventType")) {
                throw new RuntimeException("Malformed JSON received!");
            }

            event.setEventType(EventType.valueOf(jsonObject.getString("EventType")));
            jsonObject.remove("EventType");
            event.setEventTime(new Date(jsonObject.getLong("EventTime")));
            jsonObject.remove("EventTime");

            Iterator eventIterator = jsonObject.keys();
            while (eventIterator.hasNext()) {
                String key = (String) eventIterator.next();
                event.putParameter(key, jsonObject.getString(key));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(event.toString());
        return event;
    }

    /** POSTs the given JSON to the game server and returns the result */
    public static InputStream request(JSONObject jsonObject) {
        try {
            URLConnection connection = new URL(serverURL).openConnection();
            connection.setDoOutput(true); // Triggers POST.
            connection.setRequestProperty("Accept-Charset", charset);
            connection.setRequestProperty("Content-Type", "application/json;charset=" + charset);
            OutputStream output = connection.getOutputStream();
            output.write(jsonObject.toString().getBytes(charset));
            output.close();
            InputStream response = connection.getInputStream();

            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
