package com.ttheissi.navy_battle.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ttheissi.navy_battle.gameplay.GameInstance;
import com.ttheissi.navy_battle.gameplay.Player;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "db-navy-battle";

    // Contacts table name
    private static final String TABLE_GAMES = "Games";

    // Contacts Table Columns names
    private static final String KEY_GAME_UID = "gameUID";
    private static final String KEY_GAME_INSTANCE = "gameState";

    private static final int COLUMN_SIZE = 10;
    private static final int ROW_SIZE = 10;

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ORDERS_TABLE = "CREATE TABLE " + TABLE_GAMES + "(" + KEY_GAME_UID + " TEXT," + KEY_GAME_INSTANCE + ")";
        db.execSQL(CREATE_ORDERS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GAMES);
        // Create tables again
        onCreate(db);
    }

 /*   public int getMaxId() {
        int maxId = -1;
        String maxIdQuery = "SELECT MAX(idOrder) AS maxId FROM " + TABLE_GAMES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor mCursor = db.rawQuery(maxIdQuery, null);
        try {
            if (mCursor.getCount() > 0) {
                mCursor.moveToFirst();
                maxId = mCursor.getInt(0);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            // db.close();
        }
        return maxId;
    }*/

    /** Get all of the GameInstance objects from the DB */
    public GameInstance[] getGames(Player player) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_GAMES, new String[]{KEY_GAME_INSTANCE}, null, null, null, null, null);
        List<GameInstance> gameInstances = new ArrayList<GameInstance>();

        if (cursor != null) {
            cursor.moveToFirst();
            do {
                gameInstances.add(GameInstance.fromString(cursor.getString(0)));
            } while (cursor.moveToNext());

            return gameInstances.toArray(new GameInstance[0]);
        } else return new GameInstance[0];
    }

    // Add Game instance
    public void addGameInstance(GameInstance gameInstance, Player player) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i = 0; i < ROW_SIZE; i++) {
            for (int j = 0; j < COLUMN_SIZE; j++) {
                ContentValues values = new ContentValues();
                values.put(KEY_GAME_UID, gameInstance.getGameState().getGameUID());
                values.put(KEY_GAME_INSTANCE, gameInstance.toString());
                db.insert(TABLE_GAMES, null, values);
            }
        }
        db.close(); // Closing database connection
    }

    // Getting single contact
    public GameInstance getGameInstance(String gameUID) {
        SQLiteDatabase db = this.getReadableDatabase();
        GameInstance toReturn = new GameInstance();

        Cursor cursor = db.query(TABLE_GAMES, new String[]{KEY_GAME_UID, KEY_GAME_INSTANCE}, KEY_GAME_UID + "=?",
                new String[]{gameUID}, null, null, null, null
        );
        if (cursor != null)
            cursor.moveToFirst();
        else throw new RuntimeException("No Game with such UID!");

        return GameInstance.fromString(cursor.getString(1));
    }
}

    // Getting All Contacts
   /* public ArrayList<GameInstance> getAllGameInstances() {
        ArrayList<GameInstance> gamesList = new ArrayList<GameInstance>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_GAMES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            int i = 0;
            do {
                String nameEnemy = cursor.getString(1);
                String UIDenemy = cursor.getString(2);
                String gameUID = cursor.getString(3);
                String stringGameState = cursor.getString(4);
                String stringGameStatus = cursor.getString(5);

                GameInstance game = new GameInstance();
                Player enemy = new Player(nameEnemy, UUID.fromString(UIDenemy));
                GameState gameState = new GameState(stringGameState);
                GameStatus gameStatus = GameStatus.valueOf(stringGameStatus);

                game.setEnemy(enemy);
                game.setUID(gameUID);
                game.setGameState(gameState);
                game.setGameStatus(gameStatus);

                // Adding contact to list
                gamesList.add(i, game);
                i++;
            } while (cursor.moveToNext());
        }

        return gamesList;
    }
}*/

    // Updating single contact
   /* public int updateContact(Order order) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_ORDER, order.getId());
        values.put(KEY_NAME, order.getName());
        values.put(KEY_PRICE, order.getPrice());
        values.put(KEY_SIZE, order.getSize());

        // updating row
        return db.update(TABLE_ORDERS, values, KEY_ID_ORDER + " = ?",
                new String[] { String.valueOf(order.getId()) });
    }

    // Deleting single contact
    public void deleteOrder(Order order) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ORDERS, KEY_ID_ORDER + " = ?",
                new String[] { String.valueOf(order.getId()) });
        db.close();
    }

    // Getting contacts Count
    public int getOrdersCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ORDERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }

    public boolean exportDB(String path){
        String backupDBPath;
        if (path.equals(""))
            backupDBPath = DATABASE_NAME;
        else
            backupDBPath = path;
        Environment Environment;
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source=null;
        FileChannel destination=null;
        String currentDBPath = "/data/"+ "com.ttheissi.a3.foodapp" +"/databases/" + DATABASE_NAME;
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            return true;
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }
    }*/
