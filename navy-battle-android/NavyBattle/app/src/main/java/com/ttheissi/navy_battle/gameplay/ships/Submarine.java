package com.ttheissi.navy_battle.gameplay.ships;

import com.ttheissi.navy_battle.Game;
import com.ttheissi.navy_battle.R;

/**
 * Created by Thomas on 5/9/14.
 */
public class Submarine implements Placeable {

    private int rot = 0;

    public Submarine() {
        rot = Game.getRot();
    }

    @Override
    public int getSize() {
        return 3;
    }

    @Override
    public int getDrawId() {
        if (rot != 0) return R.drawable.subrot;
        else return R.drawable.sub;
    }

    @Override
    public String getDisplayName() {
        return "submarine";
    }

    @Override
    public int getRot() {
        return rot;
    }

    @Override
    public void setRot(int rot) {
        this.rot = rot;
    }
}
