package com.ttheissi.navy_battle.server;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Binder;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import com.ttheissi.navy_battle.R;
import com.ttheissi.navy_battle.database.DBHandler;
import com.ttheissi.navy_battle.gameplay.GameInstance;
import com.ttheissi.navy_battle.gameplay.GameState;
import com.ttheissi.navy_battle.gameplay.GameStatus;
import com.ttheissi.navy_battle.gameplay.Player;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;


public class UpdateRecieverService extends Service {

        private final IBinder mBinder = new ServiceBinder();
        private int length = 0;
        private ServerLayer serverLayer;
        private boolean running;
        private static final long UPDATE_FREQUENCY = 15000;
        private Player player;
        private long lastCheck;

        public UpdateRecieverService() {}

        public class ServiceBinder extends Binder {
            public UpdateRecieverService getService()
            {
                return UpdateRecieverService.this;
            }
        }

        @Override
        public IBinder onBind(Intent arg0){return mBinder;}

        @Override
        public void onCreate (){
            super.onCreate();

            lastCheck = Calendar.getInstance().getTimeInMillis();

            serverLayer = ServerLayer.getInstance();
            player = serverLayer.getPlayer();
            running = false;
        }

        @Override
        public int onStartCommand (Intent intent, int flags, int startId)
        {
            if (!running) {
                running = true;
                Toast.makeText(this, "Starting update reciever service...", Toast.LENGTH_SHORT).show();

                checker.setPriority(Thread.MIN_PRIORITY);
                checker.start();
            }

            return START_STICKY;
        }

        Thread checker = new Thread(new Runnable() {
            @Override
            public void run() {
                while (running) {
                    if (Calendar.getInstance().getTimeInMillis() - lastCheck > UPDATE_FREQUENCY) {
                        System.out.println("Getting Updates");
                        processEvents(serverLayer.getUpdates());
                        lastCheck = Calendar.getInstance().getTimeInMillis();
                    }
                }
            }
        });

        @Override
        public void onDestroy ()
        {
            super.onDestroy();
            Toast.makeText(this, "Stopping Service", Toast.LENGTH_SHORT).show();
            running = false;
        }

        private void processEvents(Event[] events) {
            if (events != null)
            for (Event event : events) {
                if (event != null)
                switch (event.getEventType()) {
                    case CreateGame:
                        // Create game instance
                        Map<String, String> eventParams = event.getEventParameters();
                        GameInstance gameInstance = new GameInstance();
                        Player enemy = new Player("DEFAULT", UUID.fromString(eventParams.get("OpponentUID")));
                        gameInstance.setEnemy(enemy);
                        gameInstance.setGameState(new GameState(eventParams.get("GameUID")));
                        gameInstance.setGameStatus(GameStatus.InProgress);

                        DBHandler dbHandler = new DBHandler(getApplicationContext());
                        dbHandler.addGameInstance(gameInstance, player);
                        System.out.println("Added game with guid " + gameInstance.getGameState().getGameUID());
                        break;
                    case QuitGame:

                        break;
                    case GameAction:

                        break;
                    case Update:
                        JSONArray jsarray = null;
                        try {
                            jsarray = new JSONArray(event.getEventParameters().get("Events"));
                            Event[] subevents = new Event[jsarray.length()];
                            for (int i = 0; i < jsarray.length(); i++) {
                                JSONObject current = jsarray.getJSONObject(0);
                                subevents[i] = new Event();

                                Iterator eventIterator = current.keys();
                                while (eventIterator.hasNext()) {
                                    String key = (String) eventIterator.next();
                                    subevents[i].putParameter(key, current.getString(key));
                                }
                            }
                            processEvents(subevents);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    default:
                        System.err.println("An event was recieved that could not be handled!");
                }
            }
        }

   }


