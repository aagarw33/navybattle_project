package com.ttheissi.navy_battle.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.ttheissi.navy_battle.CustomAdapter;
import com.ttheissi.navy_battle.Game;
import com.ttheissi.navy_battle.LaunchGame;
import com.ttheissi.navy_battle.R;
import com.ttheissi.navy_battle.gameplay.GameInstance;
import com.ttheissi.navy_battle.gameplay.GameState;
import com.ttheissi.navy_battle.gameplay.GameStatus;
import com.ttheissi.navy_battle.gameplay.Player;

import java.util.ArrayList;
import java.util.UUID;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ListFragmentPreviousOpponents extends Fragment {

    /** LIST VIEW **/
        private ListView _lv;
    /** LIST VIEW **/
    /** GAMES **/
        private ArrayList<GameInstance> _currentGames = new ArrayList<GameInstance>();
        private ArrayList<GameInstance> _previousGames = new ArrayList<GameInstance>(); //not used for the moment
    /** PLAYERS **/
    /** CUSTOM ADAPTER **/
        //private com.ttheissi.navy_battle.CustomAdapter = new com.ttheissi.navy_battle.CustomAdapter()
    /** CUSTOM ADAPTER **/
    /** ARRAY ADAPTER LIST VIEW **/
        //private ArrayAdapter<GameInstance> _arrayAdapter;
    /** ARRAY ADAPTER LIST VIEW **/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_list_fragment_previous_opponents, container, false);
    }

    public void onStart() {
        super.onStart();
        /** LISTVIEW **/
            _lv = (ListView) getActivity().findViewById(R.id.list);
        /** LISTVIEW **/
        _currentGames.clear();
        requestGamesAndAddToList();
    }

    public void requestGamesAndAddToList() {
        //CREATING DUMMY GAME INSTANCES WITH FAKE GAMESTATUS,  GAMESTATE AND ENEMY
        GameInstance first = new GameInstance();
        GameInstance second = new GameInstance();
        GameInstance third = new GameInstance();
        GameInstance fourth = new GameInstance();
        UUID UidOne = UUID.randomUUID();//UUID ENEMY 1
        UUID UidTwo = UUID.randomUUID(); //UUID ENEMY 2
        UUID UidThree = UUID.randomUUID(); //UUID ENEMY 3
        UUID UidFour = UUID.randomUUID(); //UUID ENEMY 4
        UUID gameUID1 = UUID.randomUUID(); //UUID GAME 1
        UUID gameUID2 = UUID.randomUUID(); //UUID GAME 2
        UUID gameUID3 = UUID.randomUUID(); //UUID GAME 3
        UUID gameUID4 = UUID.randomUUID(); //UUID GAME 4
        GameState gameState1 = new GameState(gameUID1.toString());
        GameState gameState2 = new GameState(gameUID2.toString());
        GameState gameState3 = new GameState(gameUID3.toString());
        GameState gameState4 = new GameState(gameUID4.toString());
        Player thomas1 = new Player("Thomas", UidOne);
        Player akash = new Player("Akash", UidTwo);
        Player thomas2 = new Player("Thomas", UidThree);
        Player aaron = new Player("Aaron", UidFour);
        String pending = "Pending";
        String inProgress = "InProgress";
        String completed = "Completed";
        GameStatus gameStatus1 = GameStatus.valueOf(pending);
        GameStatus gameStatus2 = GameStatus.valueOf(inProgress);
        GameStatus gameStatus3 = GameStatus.valueOf(completed);
        GameStatus gameStatus4 = GameStatus.valueOf(pending);
        /****** SET GAMESTATES STATICALLY *******/
        first.setEnemy(thomas1);
        first.setGameState(gameState1);
        first.setGameStatus(gameStatus1);
        second.setEnemy(akash);
        second.setGameState(gameState2);
        second.setGameStatus(gameStatus2);
        third.setEnemy(thomas2);
        third.setGameState(gameState3);
        third.setGameStatus(gameStatus3);
        fourth.setEnemy(aaron);
        fourth.setGameState(gameState4);
        fourth.setGameStatus(gameStatus4);
        /****** SET GAMESTATES STATICALLY *******/

        _currentGames.add(first);
        _currentGames.add(second);
        _currentGames.add(third);
        _currentGames.add(fourth);

        CustomAdapter adapter = new CustomAdapter(this.getActivity(), R.layout.gameitemslist, _currentGames);

        _lv.setAdapter(adapter);

        _lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent startGame = new Intent(getActivity(), LaunchGame.class);
                GameInstance gameChosen = _currentGames.get(position);
                startGame.putExtra("GameInstance", gameChosen.toString());
                startActivity(startGame);
            }
        });


    }
}
