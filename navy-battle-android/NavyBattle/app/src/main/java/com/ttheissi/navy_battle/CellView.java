package com.ttheissi.navy_battle;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by Thomas on 4/27/14.
 */
public class CellView extends View {
    Paint untouched = new Paint();
    Paint hit = new Paint();

    public CellView(Context context) {
        super(context);
    }

    protected void onDraw(Canvas canvas){
        untouched.setColor(Color.BLUE);
        untouched.setStyle(Paint.Style.FILL_AND_STROKE);
        untouched.setStrokeWidth(3);
        hit.setStyle(Paint.Style.FILL_AND_STROKE);
        hit.setStrokeWidth(3);

    }
}
