package com.ttheissi.navy_battle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by override on 4/16/14.
 */
public class Broadcast extends BroadcastReceiver {

    //Broadcast Reciver to Check Network //
    @Override
    public void onReceive(Context context, Intent intent) {
        String status = NetworkCheck.getConnectivityStatusString(context);
        Toast.makeText(context,status,Toast.LENGTH_LONG).show();
    }
}
