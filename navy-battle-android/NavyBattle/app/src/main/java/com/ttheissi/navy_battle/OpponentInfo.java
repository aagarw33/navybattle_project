package com.ttheissi.navy_battle;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.EditTextPreference;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ttheissi.navy_battle.gameplay.GameInstance;

/**
 * Created by override on 5/3/14.
 */
public class OpponentInfo extends Activity implements View.OnClickListener {

    Button ok;
    EditText username, getuid;
    GameInstance gameInstance;
    EditText opponentName;
    EditText opponentUID;
    Intent retrieveIntent;
    private boolean mIsBound = false;
    public Music mServ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opponent_info);
        Intent intent = getIntent();
        gameInstance = GameInstance.fromString(intent.getExtras().getString("GameInstance"));
        opponentName = (EditText) findViewById(R.id.eOpponentUsername);
        opponentUID = (EditText) findViewById(R.id.eOpponentUID);
        opponentName.setText(gameInstance.getEnemy().getName());
        opponentUID.setText(gameInstance.getEnemy().getUID().toString());
        ok = (Button) findViewById(R.id.bOK);
        ok.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent wat = new Intent(this, LaunchGame.class);
        wat.putExtra("GameInstance", gameInstance.toString());
        startActivity(wat);
    }

    //Background Music Life Cycle//
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startService(new Intent(this, Music.class));

    }

    @Override
    protected void onStop() {
        super.onStop();
        //  if(mServ!=null){
        doUnbindService();
        //    mServ = null;
        // if(isFinishing()){
        stopService(new Intent(this, Music.class));
        // }
        // }

    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, Music.class));
        doBindService();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    //Binding the Background music Service to the Activity
    public ServiceConnection Scon = new ServiceConnection() {

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder) binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService() {
        bindService(new Intent(this, Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            unbindService(Scon);
            mIsBound = false;
        }
    }
}

