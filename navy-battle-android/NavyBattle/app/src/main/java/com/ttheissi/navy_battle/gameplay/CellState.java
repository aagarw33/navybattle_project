package com.ttheissi.navy_battle.gameplay;

/**
 * Created by Aaron on 4/17/2014.
 */
public enum CellState {
    Hit,
    Miss,
    Water;
}
