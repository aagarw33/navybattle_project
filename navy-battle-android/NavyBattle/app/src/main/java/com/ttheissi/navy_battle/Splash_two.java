package com.ttheissi.navy_battle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MotionEvent;
import android.widget.Toast;

import com.ttheissi.navy_battle.server.UpdateRecieverService;


/**
 * Created by override on 4/4/14.
 */
public class Splash_two extends Activity {

    private Thread mSplashThread;
    final Splash_two sScreen = this;
    private boolean mIsBound = false;
    private Music mServ;
    private UpdateRecieverService serv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);


        mSplashThread = new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    synchronized (this) {

                        wait(5000);
                    }
                } catch (InterruptedException e) {

                }
                Intent i = new Intent(sScreen, MainActivity.class);
                startActivity(i);
                finish();
            }

        };
        mSplashThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            synchronized (mSplashThread) {
                mSplashThread.notifyAll();
            }
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(Splash_two.this, UpdateRecieverService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }



    public ServiceConnection serverservicecon = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            serv = ((UpdateRecieverService.ServiceBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serv = null;
        }
    };

    void doBindService() {
        // bindService(new Intent(this,Music.class),
        //     Scon,Context.BIND_AUTO_CREATE);
        bindService(new Intent(this, UpdateRecieverService.class), serverservicecon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            //unbindService(Scon);
            unbindService(serverservicecon);
            mIsBound = false;
        }
    }
}