package com.ttheissi.navy_battle.gameplay.ships;

import com.ttheissi.navy_battle.Game;
import com.ttheissi.navy_battle.R;

/**
 * Created by Thomas on 5/9/14.
 */
public class Cruiser implements Placeable {

    private int rot = 0;

    public Cruiser() {
        rot = Game.getRot();
    }

    @Override
    public int getSize() {
        return 3;
    }

    @Override
    public int getDrawId() {
        if (rot == -90) return R.drawable.cruiserrot;
        else return R.drawable.cruiser;
    }

    @Override
    public String getDisplayName() {
        return "cruiser";
    }

    @Override
    public int getRot() {
        return rot;
    }

    @Override
    public void setRot(int rot) {
        this.rot = rot;
    }
}
