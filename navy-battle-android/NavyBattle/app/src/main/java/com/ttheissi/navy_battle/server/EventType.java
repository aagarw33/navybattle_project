package com.ttheissi.navy_battle.server;

/**
 * Created by Aaron on 3/23/14.
 * <p/>
 * An enum containing each of the types of events
 */
public enum EventType {
    DEFAULT,
    CreateGame,
    QuitGame,
    GameAction,
    RandomGame,
    Update;
}
