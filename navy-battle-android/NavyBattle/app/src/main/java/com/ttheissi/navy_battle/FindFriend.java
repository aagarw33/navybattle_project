package com.ttheissi.navy_battle;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ttheissi.navy_battle.gameplay.Player;
import com.ttheissi.navy_battle.server.ServerLayer;

import java.util.UUID;

public class FindFriend extends Activity implements View.OnClickListener {

    Button submitButton;
    EditText enterUUID;
    ProgressBar load;
    private boolean mIsBound = false;
    public Music mServ;
    Animation sub,rot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friend);
        /** BUTTONS **/
            submitButton = (Button) findViewById(R.id.buttonSubmit);
            submitButton.setOnClickListener(this);
            load = (ProgressBar)findViewById(R.id.pBar);
        /** BUTTONS **/
            enterUUID = (EditText)findViewById(R.id.enterUID);
        /**Animation**/
        sub = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_submit);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.find_friend, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Background Music Life Cycle
    @Override
    public void onBackPressed() {
        super.onBackPressed();
      //  startService(new Intent(this, Music.class));

    }

    @Override
    protected void onStop() {
        super.onStop();
        //if(mServ!=null){
            doUnbindService();
          //  mServ = null;
            //if(isFinishing()){
                stopService(new Intent(this,Music.class));
           // }
      //  }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this,Music.class));
        doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }
    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.buttonSubmit:
                ServerLayer serverLayer = ServerLayer.getInstance();
                SharedPreferences prefs = getPreferences(MODE_PRIVATE);
                if (!prefs.contains("UserUID")) {
                    // there is no current player UUID
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("UserUID", UUID.randomUUID().toString());
                    editor.commit();
                }

                String puid = prefs.getString("UserUID", "");
                Player player = new Player("Test Player", UUID.fromString(puid));

                EditText uidBox = (EditText) findViewById(R.id.enterUID);
                if (uidBox.getText().toString().length() > 0) {
                    serverLayer.requestOpponent(uidBox.getText().toString());
                    load.setVisibility(View.VISIBLE);
                    load.setProgress(10);
                    finish();
                } else {
                    Toast.makeText(this, "Please enter a UID!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //Binding the Service to the Activity
   public ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
}
}
