package com.ttheissi.navy_battle.server.events;

import android.os.AsyncTask;

import com.ttheissi.navy_battle.gameplay.Player;
import com.ttheissi.navy_battle.server.Event;
import com.ttheissi.navy_battle.server.EventType;
import com.ttheissi.navy_battle.server.ServerLayer;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.Calendar;

/**
 * Created by Aaron on 3/27/2014.
 *
 * Asyncronous task to request a random opponent from the server
 */
public class RandomOpponentRequest extends AsyncTask<Player, Integer, Event> {

    // Requests a random opponent with the given
    @Override
    protected Event doInBackground(Player... player) {
        JSONObject jsonObject = new JSONObject();
        Event response = null;
        try {
            jsonObject.put("EventType", EventType.RandomGame.toString());
            jsonObject.put("PlayerUID", player[0].getUID());
            jsonObject.put("EventTime", Calendar.getInstance().getTimeInMillis());
            jsonObject.put("PlayerName", player[0].getName());

            System.out.println(jsonObject.toString());

            InputStream result = ServerLayer.request(jsonObject);
            for (int i = 0; i < 5; i++)
                if (result == null)
                    result = ServerLayer.request(jsonObject);
                else break;

            if (result != null)
                return ServerLayer.eventFromStream(result);
            else return null;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void onPostExecute(Event result) {
        if (result != null)
        ServerLayer.getInstance().returnFromAsync(result);
    }
}
