package com.ttheissi.navy_battle;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

import com.ttheissi.navy_battle.fragments.ControlsFragment;
import com.ttheissi.navy_battle.fragments.GridFragment;
import com.ttheissi.navy_battle.fragments.UserGridFragment;
import com.ttheissi.navy_battle.gameplay.GameInstance;

public class Game extends FragmentActivity {

    private static int rot = 0;
    private static int shipType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // gg = new GridView(this);
        setContentView(R.layout.activity_game_board);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        FragmentManager fm = getSupportFragmentManager();
        fm.findFragmentById(R.id.game_board);
        Intent intent = getIntent();
        if (!intent.getExtras().containsKey("GameInstance"))
            throw new RuntimeException("No Game Chosen!");

        GameInstance instance = GameInstance.fromString(intent.getExtras().getString("GameInstance"));
        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
        boolean done = false;
        if (!instance.getGameState().getShipsPlaced()){
                ft.add(R.id.game_board_top,new ControlsFragment(),"shipPlacementControls");
                ft.add(R.id.game_board,new GridFragment(),"userGrid");
                ft.commit();
                // we haven't placed ships
        }
        else{
            // ships have been placed
            ft.add(R.id.game_board_top, new GridFragment(),"userGrid");
            ft.add(R.id.game_board, new GridFragment(),"opponentGrid");
            ft.commit();
        }
    }
    public void onClick(View v) {

    }

    public static int getRot() {
        return rot;
    }

    public static void setRot(int rot) {
        Game.rot = rot;
    }

    public static int getShipType() {
        return shipType;
    }

    public static void setShipType(int shipType) {
        Game.shipType = shipType;
    }

  /* public void shipToCell(int numCells, float degrees){
        GridView.setDegrees(degrees);
       GridView.setNumCells(numCells);
       Toast.makeText(getApplicationContext(),"Wassup",Toast.LENGTH_LONG).show();
    }*/
}