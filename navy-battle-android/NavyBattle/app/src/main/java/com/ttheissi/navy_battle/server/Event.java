package com.ttheissi.navy_battle.server;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aaron on 3/23/14.
 * <p/>
 * An event that can be passed to and from the game server
 */
public class Event {
    private EventType eventType;
    private Date eventTime;
    private Map<String, String> eventParameters;

    /** Default constructor, uses current time, EventType.DEFAULT, and empty map */
    public Event() {
        eventType = EventType.DEFAULT;
        eventTime = Calendar.getInstance().getTime();
        eventParameters = new HashMap<String, String>();
    }

    /** Shortcut to create Event with predefined parameters */
    public Event(EventType eventType, Date eventTime, Map<String, String> eventParameters) {
        this.eventTime = eventTime;
        this.eventType = eventType;
        this.eventParameters = eventParameters;
    }

    public String toString() {
        return String.format("EventType: %s, EventTime: %s, Values: %s",
                eventType.toString(), eventTime.toString(), eventParameters.toString());
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public void putParameter(String name, String value) {
        eventParameters.put(name, value);
    }

    public Map<String, String> getEventParameters() {
        return eventParameters;
    }
}
