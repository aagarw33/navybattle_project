package com.ttheissi.navy_battle.gameplay.ships;

/**
 * Created by aaron on 5/9/14.
 */
public interface Placeable {
    public int getSize();
    public int getDrawId();
    public String getDisplayName();
    public int getRot();
    public void setRot(int rot);
}
