package com.ttheissi.navy_battle.fragments;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;



import com.ttheissi.navy_battle.Game;
import com.ttheissi.navy_battle.GridView;

import com.ttheissi.navy_battle.Game;
import com.ttheissi.navy_battle.R;
import com.ttheissi.navy_battle.ResultsWin;
import com.ttheissi.navy_battle.gameplay.ships.Battleship;
import com.ttheissi.navy_battle.gameplay.ships.Carrier;
import com.ttheissi.navy_battle.gameplay.ships.Cruiser;
import com.ttheissi.navy_battle.gameplay.ships.Scout;
import com.ttheissi.navy_battle.gameplay.ships.Submarine;

/**
 * Created by override on 4/20/14.
 */
public class ControlsFragment extends Fragment implements View.OnClickListener {
    float degrees;
    Game dataTransfer;
    Button five, four, three, three_sub, two,result_demo;
    int counter = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_controls, container, false);
        //Buttons
        Button rotate = (Button) v.findViewById(R.id.btn_rotate_ship);
        five = (Button) v.findViewById(R.id.btn_5_dot_ship);
         two = (Button) v.findViewById(R.id.btn_2_dot_ship);
        three = (Button) v.findViewById(R.id.btn_3_dot_ship);
        three_sub = (Button) v.findViewById(R.id.btn_3_dot_sub);
        four = (Button) v.findViewById(R.id.btn_4_dot_ship);
        result_demo = (Button)v.findViewById(R.id.btn_save_changes); //Implementing results screen on this.
        five.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        three_sub.setOnClickListener(this);
        four.setOnClickListener(this);
        rotate.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void onClick(View v) {
                if (counter == 0) {
                    five.setRotation(-90);
                    four.setRotation(-90);
                    three.setRotation(-90);
                    three_sub.setRotation(-90);
                    two.setRotation(-90);
                    Game.setRot(-90);
                    counter = 1;
                } else if (counter == 1) {
                    five.setRotation(0);
                    four.setRotation(0);
                    three.setRotation(0);
                    three_sub.setRotation(0);
                    two.setRotation(0);
                    Game.setRot(0);
                    counter = 0;
                }
            }
        });

        result_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent win = new Intent(getActivity(), ResultsWin.class);
                startActivity(win);
            }
        });
        return v;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_5_dot_ship:
                Carrier five_b = new Carrier();
                Log.i("test","5");
                degrees = five.getRotation();
                Game.setShipType(0);
        break;
            case R.id.btn_4_dot_ship:
                Battleship four_b = new Battleship();
                degrees = four.getRotation();
                Game.setShipType(1);
                break;
            case R.id.btn_3_dot_ship:
                Cruiser three_b = new Cruiser();
                degrees = three.getRotation();
                Game.setShipType(2);
                break;
            case R.id.btn_3_dot_sub:
                Submarine th_sub = new Submarine();
                degrees = three_sub.getRotation();
                Game.setShipType(3);
                break;
            case R.id.btn_2_dot_ship:
                Scout two_b = new Scout();
                degrees = two.getRotation();
                Game.setShipType(4);
            break;
    }
}
}
