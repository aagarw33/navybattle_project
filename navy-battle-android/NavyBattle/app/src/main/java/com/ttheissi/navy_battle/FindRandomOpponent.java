package com.ttheissi.navy_battle;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.ttheissi.navy_battle.gameplay.Player;
import com.ttheissi.navy_battle.server.ServerLayer;

import java.util.UUID;

public class FindRandomOpponent extends Activity {

    private boolean mIsBound = false;
    public Music mServ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_random_opponent);
        RequestRandomOpponent();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.find_random_opponent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Background Music Life Cycle.
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startService(new Intent(this, Music.class));

    }

    @Override
    protected void onStop() {
        super.onStop();
        //if(mServ!=null){
            doUnbindService();
           // mServ = null;
          //  if(isFinishing()){
                stopService(new Intent(this,Music.class));
           // }
        //}
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this,Music.class));
        doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public void RequestRandomOpponent() {
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        String puid = prefs.getString("UserUID", "");
        Player player = new Player("Test Player", UUID.fromString(puid));
        ServerLayer serverLayer = ServerLayer.getInstance();
        serverLayer.requestRandomOpponent();
    }

    //Binding the Service to the Activity
    public ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
    }
}