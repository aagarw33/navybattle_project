package com.ttheissi.navy_battle.gameplay.ships;

import com.ttheissi.navy_battle.Game;
import com.ttheissi.navy_battle.R;

/**
 * Created by aaron on 5/9/14.
 */
public class Battleship implements Placeable {

    private int rot = 0;

    public Battleship() {
        rot = Game.getRot();
    }

    @Override
    public int getSize() {
        return 3;
    }

    @Override
    public int getDrawId() {
        if (rot == -90) return R.drawable.battleshiprot;
        else return R.drawable.battleship;
    }

    @Override
    public String getDisplayName() {
        return "battleship";
    }

    @Override
    public int getRot() {
        return rot;
    }

    @Override
    public void setRot(int rot) {
        this.rot = rot;
    }
}
