package com.ttheissi.navy_battle.gameplay.ships;

import com.ttheissi.navy_battle.Game;
import com.ttheissi.navy_battle.R;

/**
 * Created by aaron on 5/9/14.
 */
public class Scout implements Placeable {

    private int rot = 0;

    public Scout() {
        rot = Game.getRot();
    }

    @Override
    public int getSize() {
        return 2;
    }

    @Override
    public int getDrawId() {
        if (rot == 0) return R.drawable.scout;
        else return R.drawable.scoutrot;
    }

    @Override
    public String getDisplayName() {
        return "scout";
    }

    @Override
    public int getRot() {
        return rot;
    }

    @Override
    public void setRot(int rot) {
        this.rot = rot;
    }
}
