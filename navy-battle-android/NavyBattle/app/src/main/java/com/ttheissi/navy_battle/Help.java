package com.ttheissi.navy_battle;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

/**
 * Created by override on 4/17/14.
 */
public class Help extends Activity {
    private boolean mIsBound = false;
    public Music mServ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
    }
    //Background Music Life Cycle
    @Override
    public void onBackPressed() {
        super.onBackPressed();
       startService(new Intent(this, Music.class));
       // mServ.resumeMusic();
    }

    @Override
    protected void onStop() {
        super.onStop();
       // if(mServ!=null){
            doUnbindService();
            //mServ = null;
          //  if(isFinishing()){
                stopService(new Intent(this,Music.class));
           // }
        //}
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this,Music.class));
        doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    //Binding the Background music Service to the Activity
    public ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
    }
}
