package com.ttheissi.navy_battle;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ttheissi.navy_battle.gameplay.GameInstance;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<GameInstance> {

    public CustomAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public CustomAdapter(Context context, int resource, ArrayList<GameInstance> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {

            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.gameitemslist, null);

        }

        GameInstance g = getItem(position);

        if (g != null) {

            TextView gameUIDTextView = (TextView) v.findViewById(R.id.gameUID);
            TextView nameEnemyTextView = (TextView) v.findViewById(R.id.nameEnemy);

            if (gameUIDTextView != null)
                gameUIDTextView.setText(g.getGameState().getGameUID());

            if (nameEnemyTextView != null)
                nameEnemyTextView.setText(g.getEnemy().getName());
        }

        return v;

    }
}
