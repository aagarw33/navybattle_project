package com.ttheissi.navy_battle.gameplay.ships;

import com.ttheissi.navy_battle.R;

/**
 * Created by aaron on 5/9/14.
 */
public class None implements Placeable {
    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public int getDrawId() {
        return R.drawable.none;
    }

    @Override
    public String getDisplayName() {
        return "water";
    }

    @Override
    public int getRot() {
        return 0;
    }

    @Override
    public void setRot(int rot) {

    }
}
