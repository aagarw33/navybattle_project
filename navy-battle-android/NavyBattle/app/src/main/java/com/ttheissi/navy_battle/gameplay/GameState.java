package com.ttheissi.navy_battle.gameplay;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Aaron on 3/24/2014.
 *
 * Contains all of the state of a particular game. For use in the GameInstance class
 */
public class GameState {
    private Cell[][] playerGrid;
    private Cell[][] enemyGrid;
    private String gameUID;
    private Date gameStartDate;
    private Date lastUpdateDate;
    private boolean shipsPlaced;

    /** Create a new game instance with the given UID */
    public GameState(String UID) {
        playerGrid = new Cell[10][10];
        enemyGrid = new Cell[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                playerGrid[i][j] = new Cell();
                enemyGrid[i][j] = new Cell();
            }
        }
        gameStartDate = lastUpdateDate = Calendar.getInstance().getTime();
        gameUID = UID;
    }

    public Cell[][] getPlayerGrid() {
        return playerGrid;
    }

    public void setPlayerGrid(Cell[][] playerGrid) {
        this.playerGrid = playerGrid;
    }

    public void setPlayerCell(int row, int col, Cell state) {
        if (row < 9 && col < 9 && row >= 0 && col >= 0)
            playerGrid[row][col] = state;
    }

    public Cell getPlayerCell(int row, int col) {
        if (row < 9 && col < 9 && row >= 0 && col >= 0)
            return playerGrid[row][col];
        else return null;
    }

    public void setEnemyCell(int row, int col, Cell state) {
        if (row < 9 && col < 9 && row >= 0 && col >= 0)
            enemyGrid[row][col] = state;
    }

    public Cell getEnemyCell(int row, int col) {
        if (row < 9 && col < 9 && row >= 0 && col >= 0)
            return enemyGrid[row][col];
        else return null;
    }

    public Cell[][] getEnemyGrid() {
        return enemyGrid;
    }

    public void setEnemyGrid(Cell[][] enemyGrid) {
        this.enemyGrid = enemyGrid;
    }

    public String getGameUID() {
        return gameUID;
    }

    public void setGameUID(String gameUID) {
        this.gameUID = gameUID;
    }

    public Date getGameStartDate() {
        return gameStartDate;
    }

    public void setGameStartDate(Date gameStartDate) {
        this.gameStartDate = gameStartDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public void setShipsPlaced(boolean shipsPlaced) { this.shipsPlaced = shipsPlaced; }

    public boolean getShipsPlaced() { return shipsPlaced; }

    /** Takes a string created by gridToString and returns a game grid */
    public Cell[][] stringToGrid(String gridString) {
        Cell[][] gameGrid = new Cell[10][];
        String[] rows = gridString.split("::");
        for (int i = 0; i < 10; i++) {
            String[] row = rows[i].split("\\+");
            gameGrid[i] = new Cell[10];
            for (int j = 0; j < 10; j++) {
                gameGrid[i][j] = new Cell(row[j]);
            }
        }

        return gameGrid;
    }

    /** Create a String representation of a game grid */
    public String gridToString(Cell[][] gameGrid) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                sb.append(gameGrid[i][j].toString());
                sb.append("+");
            }
            sb.append("::");
        }

        return sb.toString();
    }

    public static GameState fromString(String gameState) {
        String[] splitinput = gameState.split("---");
        String uid = splitinput[0];
        String startDate = splitinput[1];
        String lastUpdate = splitinput[2];
        String shipsPlaced = splitinput[3];
        String playerGrid = splitinput[4];
        String enemyGrid = splitinput[5];

        GameState result = new GameState(uid);
        result.setGameStartDate(new Date(Long.valueOf(startDate)));
        result.setLastUpdateDate(new Date(Long.valueOf(lastUpdate)));
        result.setShipsPlaced(Boolean.valueOf(shipsPlaced));
        result.setPlayerGrid(result.stringToGrid(playerGrid));
        result.setEnemyGrid(result.stringToGrid(enemyGrid));

        return result;
    }

    @Override
    public String toString() {
        return gameUID + "---" + gameStartDate.getTime() + "---" + lastUpdateDate.getTime() + "---" +
                shipsPlaced + "---" + gridToString(playerGrid) + "---" + gridToString(enemyGrid);
    }
}