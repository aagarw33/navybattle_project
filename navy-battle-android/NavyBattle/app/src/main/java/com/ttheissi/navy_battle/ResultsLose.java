package com.ttheissi.navy_battle;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by override on 4/19/14.
 */
public class ResultsLose extends Activity {
    ImageView loose;
    Animation flash;
    Button ok;
    MediaPlayer music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lose);

        flash = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.crossfade);
        loose = (ImageView)findViewById(R.id.iLoose);

        loose.startAnimation(flash);

        ok = (Button)findViewById(R.id.blOK);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(getApplicationContext(),MainActivity.class);
                main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                main.putExtra("EXIT",true);
                startActivity(main);
            }
        });
        music = MediaPlayer.create(ResultsLose.this,R.raw.gameover);
        music.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        music.release();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
