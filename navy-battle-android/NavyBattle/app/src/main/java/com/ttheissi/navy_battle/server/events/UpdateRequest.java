package com.ttheissi.navy_battle.server.events;

import android.os.AsyncTask;

import com.ttheissi.navy_battle.gameplay.Player;
import com.ttheissi.navy_battle.server.Event;
import com.ttheissi.navy_battle.server.EventType;
import com.ttheissi.navy_battle.server.ServerLayer;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.Calendar;

/**
 * Created by Aaron on 3/27/2014.
 */

public class UpdateRequest extends AsyncTask<Player, Integer, Event> {

    // Requests a random opponent with the given
    @Override
    protected Event doInBackground(Player... player) {
        JSONObject jsonObject = new JSONObject();
        Event response = null;
        try {
            jsonObject.put("EventType", EventType.Update.toString());
            jsonObject.put("PlayerUID", player[0].getUID());
            jsonObject.put("EventTime", Calendar.getInstance().getTimeInMillis());

            InputStream result = ServerLayer.request(jsonObject);
            for (int i = 0; i < 5; i++)
                if (result == null)
                    result = ServerLayer.request(jsonObject);
                else break;

            if (result != null)
                response = ServerLayer.eventFromStream(ServerLayer.request(jsonObject));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(Event result) {
        if (result == null) System.out.println("No result from update");
        else ServerLayer.getInstance().returnFromAsync(result);
    }
}
