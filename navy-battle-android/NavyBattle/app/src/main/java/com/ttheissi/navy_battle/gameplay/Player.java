package com.ttheissi.navy_battle.gameplay;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.UUID;

// simple class that just has one member property as an example
public class Player {
    private String _name;
    private UUID _UID;

    /* everything below here is for implementing Parcelable */

    public Player(String name, UUID UID){
        this._name = name;
        this._UID = UID;
    }

    public String getName() {return _name;}
    public UUID getUID() {return _UID;}
    public void setName(String name) {_name = name;}
    public void setUID(UUID UID) {_UID = UID;}

    public static Player fromString(String playerString) {
        String[] result = playerString.split("!!!");
        return new Player(result[0], UUID.fromString(result[1]));
    }

    @Override
    public String toString() {
        return _name + "!!!" + _UID.toString();
    }
}