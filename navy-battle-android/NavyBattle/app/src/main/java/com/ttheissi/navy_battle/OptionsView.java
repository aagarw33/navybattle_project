package com.ttheissi.navy_battle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.view.WindowManager.LayoutParams;

import java.util.UUID;

public class OptionsView extends Activity {

    //Background Music Variables//
    private boolean mIsBound = false;
    public Music mServ;
     ToggleButton buttonMute;
      Button buttonRate;
    int m =0;
    AudioManager am;
    int b;
    TextView guid;
    UUID uid;
    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        b = am.getStreamVolume(AudioManager.STREAM_MUSIC);
        buttonMute = (ToggleButton) findViewById(R.id.bMute);

        ToggleButton sharingButton = (ToggleButton)findViewById(R.id.bshare);
        sharingButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                shareit();
            }
        });

        buttonRate = (Button)findViewById(R.id.bRate);

        buttonRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent play_intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="));
                startActivity(play_intent);

            }
        });

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        if (!prefs.contains("UserUID")) {
            // there is no current player UUID
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("UserUID", UUID.randomUUID().toString());
            editor.commit();
        }

        //Getting UID//
         uid = UUID.fromString(prefs.getString("UserUID", ""));
         msg = uid.toString();
         guid = (TextView) findViewById(R.id.textView3);
         guid.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               showAlertDialog(OptionsView.this,"UID",msg,false);
           }
       });

      }

    public void shareit(){
        //write what you wana share
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        if (!prefs.contains("UserUID")) {
            // there is no current player UUID
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("UserUID", UUID.randomUUID().toString());
            editor.commit();
        }

        String shareBody = "Play NavyBattles with me! My UID is: " + prefs.getString("UserUID", "");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, " ");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
    //Mute Toggle Button//
    public void onToggleClicked(View v) {
        boolean on = ((ToggleButton) v).isChecked();
        SharedPreferences m_prefs = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = m_prefs.edit();

        if (on) {
            mServ.mute();
            editor.putBoolean("xyz",true);
            editor.commit();
        } else {
            mServ.unmute();
            editor.putBoolean("xyz",false);
            editor.commit();
        }
    }

    /*Alert Box*/
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        //alertDialog.setIcon(R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.options_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Background Music Life Cycle//
    @Override
    public void onBackPressed() {
        super.onBackPressed();
       startService(new Intent(this, Music.class));

    }
    @Override
    protected void onStop() {
        super.onStop();
      //  if(mServ!=null){
            doUnbindService();
        //    mServ = null;
           // if(isFinishing()){
                stopService(new Intent(this,Music.class));
           // }
       // }

    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this,Music.class));
        doBindService();
        SharedPreferences m_prefs = getPreferences(MODE_PRIVATE);
        boolean check = m_prefs.getBoolean("xyz",true);

        if(check==true){
            buttonMute.setChecked(true);
            //mServ.mute();
        }else{
            buttonMute.setChecked(false);
            //mServ.unmute();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    //Binding the Background music Service to the Activity
    public ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
    }


}
