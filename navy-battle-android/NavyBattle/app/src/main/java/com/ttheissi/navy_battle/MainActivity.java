package com.ttheissi.navy_battle;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.ttheissi.navy_battle.database.DBHandler;
import com.ttheissi.navy_battle.gameplay.Cell;
import com.ttheissi.navy_battle.gameplay.GameInstance;
import com.ttheissi.navy_battle.gameplay.GameState;
import com.ttheissi.navy_battle.gameplay.GameStatus;
import com.ttheissi.navy_battle.gameplay.Player;
import com.ttheissi.navy_battle.server.ServerLayer;
import com.ttheissi.navy_battle.server.UpdateRecieverService;
//import com.ttheissi.navy_battle.server.UpdateRecieverService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.app.PendingIntent.getService;



public class MainActivity extends FragmentActivity implements View.OnClickListener{

    /* Background Music stuff*/
     private boolean mIsBound = false;
     private Music mServ;
    private UpdateRecieverService serv;
     AudioManager am;
        Animation clock,anti_clock;

    /** BUTTONS **/
        Button buttonPlay;
        Button buttonOptions;
        Button buttonHelp;
        Button buttonQuit,servertest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<GameInstance> _gameInstances = new ArrayList<GameInstance>();

        //createDummyGAMEINSTANCES(_gameInstances);
        Player thisPlayer = new Player("Thomas", UUID.randomUUID()); //Player in shared preferences

        DBHandler db = new DBHandler(this);
//        db.addGameInstance(_gameInstances.get(1), thisPlayer); //adding
        //GameInstance thisGame = db.getGameInstance(_gameInstances.get(1).getGameState().getGameUID());
        

        /** BUTTONS **/
            buttonPlay = (Button) findViewById(R.id.buttonPlay);
            buttonPlay.setOnClickListener(this);
            buttonOptions = (Button) findViewById(R.id.buttonOptions);
            buttonOptions.setOnClickListener(this);
            buttonHelp = (Button) findViewById(R.id.buttonHelp);
            buttonHelp.setOnClickListener(this);
            buttonQuit = (Button) findViewById(R.id.buttonQuit);
            buttonQuit.setOnClickListener(this);
           // servertest= (Button)findViewById(R.id.button);
            //servertest.setOnClickListener(this);

            /*Animations Stuff*/
            clock = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_anim);
            anti_clock = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_options);
            buttonPlay.startAnimation(clock);
            buttonOptions.startAnimation(anti_clock);
            buttonHelp.startAnimation(clock);
            buttonQuit.startAnimation(anti_clock);
            /** BUTTONS **/

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        if (!prefs.contains("UserUID")) {
            // there is no current player UUID
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("UserUID", UUID.randomUUID().toString());
            editor.commit();
        }

        String puid = prefs.getString("UserUID", "");
        Player player = new Player("DEFAULT_PLAYER_NAME", UUID.fromString(puid));
        ServerLayer.getInstance().init(player);
        ServerLayer serverLayer = ServerLayer.getInstance();
        serverLayer.getUpdates();
    }


    /*Alert Box*/
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
      alertDialog.setIcon(R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode==KeyEvent.KEYCODE_BACK){
            stopService(new Intent(this, Music.class));
            stopService(new Intent(this, UpdateRecieverService.class));
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onResume() {
        super.onResume();
        doBindService();
        startService(new Intent(this, Music.class));
       // startService(new Intent(this, UpdateRecieverService.class));
    }


    @Override
    protected void onStop() {
        super.onStop();
            doUnbindService();
               stopService(new Intent(this,Music.class));

    }

    @Override
    protected void onPause() {
        super.onPause();
   if(this.isFinishing())
    {
        stopService(new Intent(this,Music.class));
        doUnbindService();
    }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(View view) { //DO NOT FORGET TO BREAK AT THE END OF EACH STATEMENT
        switch(view.getId()) {
            case R.id.buttonPlay:
                String status = NetworkCheck.getConnectivityStatusString(this);
                if(status=="Not connected to Internet"){
                    showAlertDialog(MainActivity.this,"Not Connected:","Please connect your device to the Internet.",false);
                }else {
                    Toast.makeText(MainActivity.this,status,Toast.LENGTH_SHORT).show();
                    Intent chooseOpponentView = new Intent(this, ChooseOpponent.class);
                    startActivity(chooseOpponentView);
                }
                break;

            case R.id.buttonOptions:
                Intent optionsintent = new Intent(this, OptionsView.class);
                startActivity(optionsintent);
                break;

            case R.id.buttonHelp:
                Intent helpintent = new Intent(this,Help.class);
                startActivity(helpintent);
                break;

            case R.id.buttonQuit:
            if(getIntent().getBooleanExtra("EXIT",false)) {
                stopService(new Intent(this, Music.class));
                stopService(new Intent(this, UpdateRecieverService.class));
                doUnbindService();
                finish();
            }else{
                stopService(new Intent(this, Music.class));
               stopService(new Intent(this, UpdateRecieverService.class));
                doUnbindService();
                System.exit(0);
            }
                break;
            /*case R.id.button:
                SharedPreferences prefs = getPreferences(MODE_PRIVATE);
                if (!prefs.contains("UserUID")) {
                    // there is no current player UUID
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("UserUID", UUID.randomUUID().toString());
                    editor.commit();
                }

                String puid = prefs.getString("UserUID", "");
                Player player = new Player("Test Player", UUID.fromString(puid));
                ServerLayer serverLayer = ServerLayer.getInstance();
                serverLayer.requestRandomOpponent(player);
                serverLayer.getUpdates(player);
                break;*/
        }
    }


///////////*Background Music Stuff*////////////////////////
    public ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

   public ServiceConnection serverservicecon = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            serv = ((UpdateRecieverService.ServiceBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serv = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon,Context.BIND_AUTO_CREATE);
        bindService(new Intent(this, UpdateRecieverService.class), serverservicecon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
          unbindService(serverservicecon);
            mIsBound = false;
        }
    }

  /*  public void createDummyGAMEINSTANCES(ArrayList<GameInstance> gameInstances) {
        //CREATING DUMMY GAME INSTANCES WITH FAKE GAMESTATUS,  GAMESTATE AND ENEMY
        GameInstance first = new GameInstance();
        GameInstance second = new GameInstance();
        GameInstance third = new GameInstance();
        GameInstance fourth = new GameInstance();
        UUID UidOne = UUID.randomUUID();//UUID ENEMY 1
        UUID UidTwo = UUID.randomUUID(); //UUID ENEMY 2
        UUID UidThree = UUID.randomUUID(); //UUID ENEMY 3
        UUID UidFour = UUID.randomUUID(); //UUID ENEMY 4
        UUID gameUID1 = UUID.randomUUID(); //UUID GAME 1
        UUID gameUID2 = UUID.randomUUID(); //UUID GAME 2
        UUID gameUID3 = UUID.randomUUID(); //UUID GAME 3
        UUID gameUID4 = UUID.randomUUID(); //UUID GAME 4
        GameState gameState1 = new GameState(gameUID1.toString());
        GameState gameState2 = new GameState(gameUID2.toString());
        GameState gameState3 = new GameState(gameUID3.toString());
        GameState gameState4 = new GameState(gameUID4.toString());
        Player thomas1 = new Player("Thomas", UidOne);
        Player akash = new Player("Akash", UidTwo);
        Player thomas2 = new Player("Thomas", UidThree);
        Player aaron = new Player("Aaron", UidFour);
        String pending = "Pending";
        String inProgress = "InProgress";
        String completed = "Completed";
        GameStatus gameStatus1 = GameStatus.valueOf(pending);
        GameStatus gameStatus2 = GameStatus.valueOf(inProgress);
        GameStatus gameStatus3 = GameStatus.valueOf(completed);
        GameStatus gameStatus4 = GameStatus.valueOf(pending);

        Cell[][] playerGrid = new Cell[10][10];
        Cell[][] enemyGrid = new Cell[10][10];

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                playerGrid[i][j] = new Cell();
                enemyGrid[i][j] = new Cell();
            }
        }

        /****** SET GAMESTATES STATICALLY *******/
      /*  first.setEnemy(thomas1);
        first.setGameState(gameState1);
        first.setGameStatus(gameStatus1);
        first.getGameState().setPlayerGrid(playerGrid);
        first.getGameState().setEnemyGrid(enemyGrid);
        second.setEnemy(akash);
        second.setGameState(gameState2);
        second.setGameStatus(gameStatus2);
        second.getGameState().setPlayerGrid(playerGrid);
       /* second.getGameState().setEnemyGrid(enemyGrid);
        third.setEnemy(thomas2);
        third.setGameState(gameState3);
        third.setGameStatus(gameStatus3);
        third.getGameState().setPlayerGrid(playerGrid);
        third.getGameState().setEnemyGrid(enemyGrid);
        fourth.setEnemy(aaron);
        fourth.setGameState(gameState4);
        fourth.setGameStatus(gameStatus4);
        fourth.getGameState().setPlayerGrid(playerGrid);
        fourth.getGameState().setEnemyGrid(enemyGrid);
        /****** SET GAMESTATES STATICALLY *******/
       /* gameInstances.add(first);
        gameInstances.add(second);
        gameInstances.add(third);
        gameInstances.add(fourth);
    }
*/
}
