from flask import Flask
from flask import request
import json
import threading
import time
import string
import uuid

# The server address will be http://navybattles.morerunes.com

app = Flask(__name__)
#app.debug = True

# Global Queues and Events
randQueue = []
playerEvents = {}
gameEvents = {}
global server_stop

pairLock = threading.Lock()

@app.route('/', methods=['GET','POST'])
def update():
	if request.method == 'POST':
		js = request.get_json()
		if ('EventType' in js):
			eventType = js['EventType']
			if (eventType == 'CreateGame'):
				return requestGame(js)
			elif (eventType == "QuitGame"):
				return quitGame(js)
			elif (eventType == "CompleteGame"):
				return completeGame(js)
			elif (eventType == "AttackLocation"):
				return attackLocation(js)
			elif (eventType == "RandomGame"):
				print("Getting random game...")
				return randomGame(js)
			elif (eventType == "Update"):
				return update(js)
		else:
			print("Malformed JSON intercepted!")
	return '{"EventType":"DEFAULT"}'

def requestGame(js):
	playerUID = js['PlayerUID']
	playerName = js['PlayerName']
	opponentUID = js['OpponentUID']
	
	gameUID = uuid.uuid4()
	
	if (playerUID not in playerEvents):
		playerEvents[playerUID] = []
	playerEvents[playerUID].append({'EventType':'CreateGame', 'OpponentUID':opponentUID, 'GameUID':str(gameUID), 'EventTime':str(time.time())})
	
	if (opponentUID not in playerEvents):
		playerEvents[opponentUID] = []
	
	playerEvents[opponentUID].append({'EventType':'CreateGame', 'OpponentUID':playerUID, 'GameUID':str(gameUID), 'EventTime':str(time.time())})
	
	return 'success'

def quitGame(js):
	return '{"EventType":"DEFAULT"}'

def completeGame(js):
	return '{"EventType":"DEFAULT"}'

def attackLocation(js):
	return '{"EventType":"DEFAULT"}'

def update(js):
	global playerEvents, gameEvents
	puid = 0
	guid = 0
	
	if ('PlayerUID' in js):
		puid = js['PlayerUID']
	if ('GameUID' in js):
		guid = js['GameUID']
	
	if (puid == 0 and guid == 0):
		# no player or game uids given
		print("No UIDs given!")
		return '{"Events":[]}'
	
	js = {}
	events = []
	
	if (puid != 0 and puid in playerEvents):
		while len(playerEvents[puid]) > 0:
			events.append(playerEvents[puid].pop())
	elif (guid != 0 and guid in gameEvents):
		while len(gameEvents[guid]) > 0:
			events.append(gameEvents[guid].pop())
	
	js["Events"] = events
	js["EventType"] = "Update"
	js["EventTime"] = time.time()
	
	return json.dumps(js)

def randomGame(js):
	global randQueue
	if ('PlayerUID' in js):
		if (not (js['PlayerUID'] in randQueue)):
			randQueue.append(js['PlayerUID'])
		return '{"Result":"Success"}'
	else:
		return '{"Result":"Error"}'

class queueManager(threading.Thread):
	def run(self):
		global randQueue, gameEvents, playerEvents, server_stop
		while (not server_stop):
			# check for queued random players
			#print("Queue Size: {}".format(len(randQueue)))
			if (len(randQueue) >= 2):
				# create an event for each player
				p1event = {}
				p2event = {}
				guid = uuid.uuid4()
				p1uid = randQueue.pop()
				p2uid = randQueue.pop()
				
				# populate p1event
				p1event["EventType"] = "CreateGame"
				p1event["EventTime"] = time.time()
				p1event["GameUID"] = guid
				p1event["PlayerUID"] = p1uid
				p1event["EnemyUID"] = p2uid
				
				# populate p2event
				p2event["EventType"] = "CreateGame"
				p2event["EventTime"] = time.time()
				p2event["GameUID"] = guid
				p2event["PlayerUID"] = p2uid
				p2event["EnemyUID"] = p1uid
				
				if (not (p1uid in playerEvents)):
					playerEvents[p1uid] = []
				if (not (p2uid in playerEvents)):
					playerEvents[p2uid] = []
				
				playerEvents[p1uid].append(p1event)
				playerEvents[p2uid].append(p2event)
			
			# sleep so we don't burn out
			time.sleep(5)

if __name__ == "__main__":
	global server_stop
	server_stop = False
	queueManager().start()
	app.run(host='0.0.0.0', port=80)
	server_stop = True
