public enum EventType {
	DEFAULT,
	RequestGame,
	QuitGame,
	CompleteGame,
	AttackLocation;
}