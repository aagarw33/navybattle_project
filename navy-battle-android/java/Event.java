import java.util.HashMap;
import java.util.Map;

public class Event {
	private EventType eventType;
	private Map eventData;

	public Event() {
		// create an empty event
		eventType = EventType.DEFAULT;
		eventData = new HashMap<String,String>();
	}
	
	public Event(EventType type) {
		eventType = type;
		eventData = new HashMap<String,String>();
	}
	
	public EventType getEventType() {
		return eventType;
	}
	
	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}
	
	public void addEventData(String key, String value) {
		eventData.put(key, value);
	}
	
	public void getEventData() {
		return eventData;
	}
}